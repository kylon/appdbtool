# App.db Tool

Original dev: luck

### Features
* Export and import your LiveArea layout
* Wipe database without loosing your LiveArea
* Refresh database

#### Menu
* __Export and wipe__: Export your current LA layout and let the system create a new app.db
* __Export [no wipe]__: Export your current LA layout
* __Import__: Apply your backed up LA settings to the system app.db
* __Rebuild__: Trigger a database update (No changes to your LA) 

### Notes
What makes this app different is that it will __NOT__ backup/restore your app.db!  

It will export/import your LiveArea settings instead,  
so that you will not get stuck in a loop if your previous app.db was damaged,  
and you will not loose your LA layout.  

This is __NOT__ a classic app.db backup/restore app.

### Clone this repo
`git clone --recurse-submodules https://kylon@bitbucket.org/kylon/appdbtool.git`