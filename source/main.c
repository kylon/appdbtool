#include <psp2/kernel/processmgr.h>
#include <psp2/ctrl.h>
#include <psp2/power.h>
#include <vita2d.h>

#include "include/utils.h"

static const char *title = "App.db Tool 6.5 - kylon  [Original dev: luck]";
static const char *opts[] = {
        "Export LiveArea settings [wipe database]",
        "Export LiveArea settings [No wipe]",
        "Import LiveArea settings",
        "Rebuild database [No wipe]",
};

void draw_dialog(int type, vita2d_pgf *font, const char *msg) {
    float rectH = vita2d_pgf_text_height(font, 1.2f, msg) + 120;
    float rectW = vita2d_pgf_text_width(font, 1.2f, msg) + 60;
    float rectX = (SCREEN_WIDTH / 2) - (rectW / 2);
    float rectY = (SCREEN_HEIGHT / 2) - (rectH / 2);

    vita2d_draw_rectangle(rectX, rectY, rectW, rectH, RGBA8(240, 240, 240, 255));

    switch (type) {
        case 1: { // alert
            float txX = rectX + (rectW / 2) - (vita2d_pgf_text_width(font, 1.2f, "x Ok") / 2);
            float txY = rectY + rectH - vita2d_pgf_text_height(font, 1.2f, "x Ok");

            vita2d_pgf_draw_text(font, rectX + 30, rectY + 30, RGBA8(0, 0, 0, 255), 1.2f, msg);
            vita2d_pgf_draw_text(font, txX, txY, RGBA8(0, 0, 0, 255), 1.2f, "x Ok");
        }
            break;
        case 2: { // confirm
            float txXY = rectX + rectW - vita2d_pgf_text_width(font, 1.2f, "x Yes") - 10;
            float txYY = rectY + rectH - vita2d_pgf_text_height(font, 1.2f, "x Yes");
            float txYN = rectY + rectH - vita2d_pgf_text_height(font, 1.2f, "o No");

            vita2d_pgf_draw_text(font, rectX + 30, rectY + 30, RGBA8(0, 0, 0, 255), 1.2f, msg);
            vita2d_pgf_draw_text(font, rectX + 10, txYN, RGBA8(0, 0, 0, 255), 1.2f, "o No");
            vita2d_pgf_draw_text(font, txXY, txYY, RGBA8(0, 0, 0, 255), 1.2f, "x Yes");
        }
            break;
        case 3: { // progress
            float txY = rectY + (rectH / 2) - (vita2d_pgf_text_height(font, 1.2f, "Please wait...") / 2);

            vita2d_pgf_draw_text(font, rectX + 10, txY, RGBA8(0, 0, 0, 255), 1.2f, "Please wait...");
        }
            break;
        default:
            break;
    }

    return;
}

void draw_main(vita2d_pgf *font, int curr, int msgT, const char *msg) {
    float titleX = (SCREEN_WIDTH / 2) - (vita2d_pgf_text_width(font, 1.6f, title) / 2);
    float rectWidth = SCREEN_WIDTH - 60;

    vita2d_start_drawing();
    vita2d_clear_screen();

    vita2d_pgf_draw_text(font, titleX, 40, RGBA8(61, 98, 118, 255), 1.6f, title);

    for (int i = 0, y = 120; i < OPTIONS_END; ++i) {
        float textH = vita2d_pgf_text_height(font, 1.4f, opts[i]);
        float rectH = textH + 40;
        float rectY = y - (textH / 2) - 20;
        float textY = rectY + textH + (rectH / 4);

        if (curr == i)
            vita2d_draw_rectangle(30, rectY, rectWidth, rectH, RGBA8(70, 40, 244, 255));

        vita2d_pgf_draw_text(font, 50, textY, RGBA8(255, 255, 255, 255), 1.4f, opts[i]);

        y += rectH + 10;
    }

    if (msgT)
        draw_dialog(msgT, font, msg);

    vita2d_end_drawing();
    vita2d_wait_rendering_done();
    vita2d_swap_buffers();

    return;
}

int main() {
    int draw = 1, selctd = 0, msgT = 0;
    SceCtrlData pad, oldPad;
    char msg[512] = {0};
    size_t len = sizeof(char) * 512;
    vita2d_pgf *pgf;
    int ret;

    ret = init();

    if (!ret) {
        sceKernelExitProcess(0);
        return 0;
    }

    memset(&pad, 0, sizeof(pad));
    memset(&oldPad, 0, sizeof(oldPad));

    vita2d_init();

    pgf = vita2d_load_default_pgf();

    while (1) {
        sceCtrlPeekBufferPositive(0, &pad, 1);

        if (msgT) {
            if (HOLD_END(oldPad, pad, SCE_CTRL_CIRCLE)) {
                draw = 1;
                msgT = 0;
                sceKernelDelayThread(100000);

            } else if (HOLD_END(oldPad, pad, SCE_CTRL_CROSS)) {
                draw = 1;
                msgT = msgT > 1 ? 3 : 0;
                sceKernelDelayThread(120000);
            }

        } else if (IS_BTN(pad, SCE_CTRL_DOWN) && selctd + 1 < OPTIONS_END) {
            draw = 1;
            ++selctd;
            sceKernelDelayThread(170000);

        } else if (IS_BTN(pad, SCE_CTRL_UP) && selctd - 1 >= 0) {
            draw = 1;
            --selctd;
            sceKernelDelayThread(170000);

        } else if (HOLD_END(oldPad, pad, SCE_CTRL_CROSS)) {
            draw = 1;
            msgT = 2;

            switch (selctd) {
                case EXPORT_WIPE:
                    snprintf(msg, len, "Export LiveArea settings and wipe database");
                    break;
                case EXPORT_LVA:
                    snprintf(msg, len, "Export LiveArea settings");
                    break;
                case IMPORT_LVA:
                    snprintf(msg, len, "Import LiveArea settings");
                    break;
                case REBUILD_DB:
                    snprintf(msg, len, "Rebuild database [No wipe]");
                    break;
                default:
                    snprintf(msg, len, "Unknown option");
                    break;
            }

            sceKernelDelayThread(100000);
        }

        oldPad = pad;

        if (draw)
            draw_main(pgf, selctd, msgT, msg);

        draw = 0;

        if (msgT == 3) {
            msgT = 0;
            draw = 1;

            switch (selctd) {
                case EXPORT_WIPE:
                case REBUILD_DB: {
                    ret = manageDb(selctd, msg, len);

                    if (!ret) {
                        msgT = 1;
                        break;
                    }

                    scePowerRequestColdReset();
                    goto appdb_exit;
                }
                    break;
                case EXPORT_LVA: {
                    ret = manageDb(EXPORT_LVA, msg, len);
                    msgT = 1;

                    if (ret)
                        snprintf(msg, len, "Success!");
                }
                    break;
                case IMPORT_LVA: {
                    ret = importLASett(msg, len);

                    if (!ret) {
                        msgT = 1;
                        break;
                    }

                    manageDb(REBUILD_DB, msg, len);
                    scePowerRequestColdReset();
                    goto appdb_exit;
                }
                    break;
                default:
                    break;
            }
        }
    }

appdb_exit:
    vita2d_fini();
    vita2d_free_pgf(pgf);

    sceKernelExitProcess(0);
    return 0;
}
