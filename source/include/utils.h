#pragma once

#include "file.h"

#define APPDB       "ur0:shell/db/app.db"
#define APPDBT_PATH "ux0:data/appdbtool"
#define APPDB_BAK   APPDBT_PATH"/orig.db"
#define ICO_PATH    APPDBT_PATH"/icons_pos.txt"
#define PID_PATH    APPDBT_PATH"/pages_pos.txt"
#define ADB_PATH    APPDBT_PATH"/app_restored.db"
#define LOGF_PATH   APPDBT_PATH"/log.txt"

#define HOLD_END(oldPad, pad, control) ((oldPad.buttons & control) && (!(pad.buttons & control)))
#define IS_BTN(pad, control) (pad.buttons & control)

#define SCREEN_WIDTH 960
#define SCREEN_HEIGHT 540

enum options {
    EXPORT_WIPE = 0,
    EXPORT_LVA,
    IMPORT_LVA,
    REBUILD_DB,

    OPTIONS_END
};

int init();
int manageDb(int cmd, char *error, size_t len);
int exportLASett(int cmd, char *error, size_t len);
int importLASett(char *error, size_t len);
