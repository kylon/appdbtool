#pragma once

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <psp2/io/dirent.h>
#include <psp2/io/fcntl.h>
#include <psp2/io/stat.h>

int exists(const char *path);
int touchFile(const char *path);
int isDir(const char *path);
int copyFile(const char *src, const char *dest);
int delete(const char *path);
