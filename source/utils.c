#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "include/utils.h"
#include "sqlite3.h"
#include "base64.h"

#define IMPORT_ICONS_BUF_LEN 50*1024

int init() {
    int ret = 1;

    if (!exists(APPDBT_PATH))
        ret = (sceIoMkdir(APPDBT_PATH, 0777) >= 0);

    return ret;
}

int manageDb(int cmd, char *msgEr, size_t len) {
    switch (cmd) {
        case REBUILD_DB:
            return delete("ux0:id.dat");
        case EXPORT_LVA:
        case EXPORT_WIPE:
            return exportLASett(cmd, msgEr, len);
        default:
            break;
    }

    return 0;
}

int exportLASett(int cmd, char *msgEr, size_t len) {
    const char *sqlCountPages = "SELECT COUNT(*) FROM tbl_appinfo_page WHERE pageNo >= 0";
    const char *sqlSaveAppsPos = "SELECT pageId, pos, title, titleId, reserved01, quote(reserved05) FROM tbl_appinfo_icon";
    const char *sqlSavePageId = "SELECT pageId, pageNo, reserved01 FROM tbl_appinfo_page";
    char *pathToCheck[3] = {ICO_PATH, PID_PATH, "ux0:iconlayout.ini.bak"};
    FILE *iconsPosBak = NULL, *pageIdBak = NULL;
    sqlite3 *db = NULL;
    sqlite3_stmt *stmt = NULL;
    int ret = 0;

    for (int i = 0; i < 3; ++i) {
        if (!delete(pathToCheck[i]))
            return 0;
    }

    iconsPosBak = fopen(ICO_PATH, "w");
    pageIdBak = fopen(PID_PATH, "w");

    if (!iconsPosBak || !pageIdBak) {
        snprintf(msgEr, len, "export: cannot create files");
        goto exit;
    }

    ret = sqlite3_open(APPDB, &db);
    if (ret != SQLITE_OK) {
        snprintf(msgEr, len, "export: cannot open app.db");
        ret = 0;
        goto exit;
    }

    // Pages Count
    ret = sqlite3_prepare_v2(db, sqlCountPages, -1, &stmt, 0);
    if (ret != SQLITE_OK) {
        snprintf(msgEr, len, "export: sqlite error [pages]");
        ret = 0;
        goto exit;
    }

    if (sqlite3_step(stmt) == SQLITE_ROW)
        fprintf(iconsPosBak, "%s %d %d %lu %s %d %s\n", "pg", sqlite3_column_int(stmt, 0), -1, 0, "(null)", -1, "(null)");

    sqlite3_finalize(stmt);

    // Save positions
    ret = sqlite3_prepare_v2(db, sqlSaveAppsPos, -1, &stmt, 0);
    if (ret != SQLITE_OK) {
        snprintf(msgEr, len, "export: sqlite error [positions]");
        ret = 0;
        goto exit;
    }

    while (sqlite3_step(stmt) == SQLITE_ROW) {
        char title[250] = {0};
        char *b64Title = NULL;
        size_t b64StrSz, titleLen;

        sqlite3_snprintf(sizeof(title), title, "%s", sqlite3_column_text(stmt, 2));

        titleLen = strlen(title);
        b64StrSz = sizeof(char) * (titleLen * 2);
        b64Title = malloc(b64StrSz);

        memset(b64Title, 0, b64StrSz);
        bintob64(b64Title, title, titleLen);

        fprintf(iconsPosBak, "%s %d %d %lu %s %d %s\n",
                sqlite3_column_text(stmt, 3), sqlite3_column_int(stmt, 0),
                sqlite3_column_int(stmt, 1), titleLen, b64Title, sqlite3_column_int(stmt, 4),
                (char *) sqlite3_column_blob(stmt, 5));

        free(b64Title);
    }

    sqlite3_finalize(stmt);

    // Save Page IDs
    ret = sqlite3_prepare_v2(db, sqlSavePageId, -1, &stmt, 0);
    if (ret != SQLITE_OK) {
        snprintf(msgEr, len, "export: sqlite error [IDs]");
        ret = 0;
        goto exit;
    }

    while (sqlite3_step(stmt) == SQLITE_ROW) {
        fprintf(pageIdBak, "%d %d %d\n",
                sqlite3_column_int(stmt, 0), sqlite3_column_int(stmt, 1), sqlite3_column_int(stmt, 2));
    }

    sqlite3_finalize(stmt);

    ret = copyFile("ux0:iconlayout.ini", "ux0:iconlayout.ini.bak");
    if (!ret) {
        snprintf(msgEr, len, "export: iconlayout.ini error");
        goto exit;
    }

    if (cmd == EXPORT_WIPE && !delete(APPDB)) {
        snprintf(msgEr, len, "export1: app.db error");
        ret = 0;
    }

exit:
    sqlite3_close(db);
    fclose(iconsPosBak);
    fclose(pageIdBak);

    return ret;
}

int importLASett(char *msgEr, size_t len) {
    const char *sqlP0[] = {
            "BEGIN TRANSACTION",
            "PRAGMA foreign_keys = off",
            "create table tbl_appinfo_icon_n as select * from tbl_appinfo_icon",
            "create table tbl_appinfo_page_n as select * from tbl_appinfo_page",
            "drop table tbl_appinfo_icon",
            "drop table tbl_appinfo_page"
    };
    const char *sqlP1[] = {
            "CREATE TABLE tbl_appinfo_page(pageId INTEGER PRIMARY KEY NOT NULL ,pageNo INT NOT NULL,themeFile TEXT,bgColor INT,texWidth INT,texHeight INT,imageWidth INT,imageHeight INT,reserved01,reserved02,reserved03,reserved04,reserved05)",
            "CREATE INDEX idx_page_no ON tbl_appinfo_page ( pageNo )",
            "insert into tbl_appinfo_page select * from tbl_appinfo_page_n",
            "drop table tbl_appinfo_page_n",
            "CREATE TABLE tbl_appinfo_icon(pageId REFERENCES tbl_appinfo_page(pageId) ON DELETE RESTRICT NOT NULL,pos INT NOT NULL,iconPath TEXT,title TEXT COLLATE NOCASE,type NOT NULL,command TEXT, titleId TEXT,icon0Type NOT NULL,parentalLockLv INT,status INT,reserved01,reserved02,reserved03,reserved04,reserved05,PRIMARY KEY(pageId, pos))",
            "CREATE INDEX idx_icon_pos ON tbl_appinfo_icon ( pos, pageId )",
            "CREATE INDEX idx_icon_title ON tbl_appinfo_icon (title, titleId, type)",
            "insert into tbl_appinfo_icon select * from tbl_appinfo_icon_n",
            "drop table tbl_appinfo_icon_n",
            "CREATE TRIGGER tgr_deletePage2 AFTER DELETE ON tbl_appinfo_page WHEN OLD.pageNo >= 0 BEGIN UPDATE tbl_appinfo_page SET pageNo = pageNo - 1 WHERE tbl_appinfo_page.pageNo > OLD.pageNo; END",
            "CREATE TRIGGER tgr_insertPage2 BEFORE INSERT ON tbl_appinfo_page WHEN NEW.pageNo >= 0 BEGIN UPDATE tbl_appinfo_page SET pageNo = pageNo + 1 WHERE tbl_appinfo_page.pageNo >= NEW.pageNo; END",
            "PRAGMA foreign_keys = on",
            "COMMIT"
    };
    FILE *iconsPosBak = NULL, *pageIdBak = NULL, *logf = NULL;
    size_t buffSz = sizeof(char) * IMPORT_ICONS_BUF_LEN;
    size_t sqlSz = sizeof(char) * 40500;
    size_t reserved05Sz = sizeof(char) * 40000;
    char sqlPgNum[256] = {0};
    char *buff = NULL;
    char *sql = NULL;
    char *reserved05 = NULL;
    char *error = NULL;
    sqlite3 *db = NULL;
    int curFileLine = 0;
    int exitError = 0;
    int pgNumb, ret;

    if (!exists(ICO_PATH) || !exists(PID_PATH)) {
        snprintf(msgEr, len, "import: no live area settings found!");
        return 1;
    }

    if (!delete(ADB_PATH)) {
        snprintf(msgEr, len, "import: cannot delete old files");
        return 0;
    }

    if (!copyFile(APPDB, APPDB_BAK)) {
        snprintf(msgEr, len, "import: failed to backup app.db");
        return 0;
    }

    buff = malloc(buffSz);
    reserved05 = malloc(reserved05Sz);
    sql = malloc(sqlSz);
    iconsPosBak = fopen(ICO_PATH, "r");
    pageIdBak = fopen(PID_PATH, "r");
    logf = fopen(LOGF_PATH, "w+");

    if (!buff || !reserved05 || !sql) {
        snprintf(msgEr, len, "import icons: failed to malloc memory");
        exitError = 1;
        goto exit;
    }

    if (!iconsPosBak || !pageIdBak) {
        snprintf(msgEr, len, "import: cannot read exported settings files");
        exitError = 1;
        goto exit;
    }

    if (!logf) {
        snprintf(msgEr, len, "import: unable to create log file");
        exitError = 1;
        goto exit;
    }

    ret = sqlite3_open(APPDB, &db);
    if (ret != SQLITE_OK) {
        snprintf(msgEr, len, "import: cannot open app.db");
        exitError = 1;
        goto exit;
    }

    // Prepare the database
    for (int i = 0; i < 6; ++i) {
        ret = sqlite3_exec(db, sqlP0[i], NULL, NULL, &error);
        if (ret != SQLITE_OK) {
            snprintf(msgEr, len, "import: sql error: 0-%d\n\n%s", i, error);
            sqlite3_free(error);
            exitError = 1;
            goto exit;
        }
    }

    memset(buff, 0, buffSz);

    while (fgets(buff, IMPORT_ICONS_BUF_LEN, iconsPosBak) != NULL) {
        char id[11] = {0};
        char b64Title[500] = {0};
        int scanfC, pageId, pos, reserved01;
        size_t decTitleLen;

        memset(sql, 0, sqlSz);
        memset(reserved05, 0, reserved05Sz);

        scanfC = sscanf(buff, "%s %d %d %lu %s %d %s", id, &pageId, &pos, &decTitleLen, b64Title, &reserved01, reserved05);
        if (scanfC != 7) {
            snprintf(msgEr, len, "import icons: failed to parse line: %d", curFileLine);
            exitError = 1;
            goto exit;
        }

        ++curFileLine;

        if (strcmp(id, "pg") == 0) {
            pgNumb = pageId;
            continue;
        }

        if (strcmp(id, "(null)") == 0) {
            sqlite3_stmt *stmt = NULL;
            char sqlCountFold[256] = {0};
            int exists = 0;

            snprintf(sqlCountFold, sizeof(sqlCountFold), "SELECT COUNT(*) FROM tbl_appinfo_icon_n WHERE reserved01 = %d", reserved01);

            ret = sqlite3_prepare_v2(db, sqlCountFold, -1, &stmt, 0);
            if (ret != SQLITE_OK)
                continue;

            if (sqlite3_step(stmt) == SQLITE_ROW)
                exists = sqlite3_column_int(stmt, 0);

            sqlite3_finalize(stmt);

            if (!exists) {
                char *titleEnd = (char *) b64decode(b64Title);

                if (!titleEnd) {
                    snprintf(msgEr, len, "import icons: failed to decode title string");
                    exitError = 1;
                    goto exit;
                }

                *titleEnd = '\0';
                if (strlen(b64Title) != decTitleLen) {
                    snprintf(msgEr, len, "import icons: title length mismatch: encode/decode error");
                    exitError = 1;
                    goto exit;
                }

                sqlite3_snprintf(sqlSz, sql,
                                 "INSERT INTO tbl_appinfo_icon_n (reserved05, reserved04, reserved03, reserved02, reserved01, status, parentalLockLv, icon0Type, titleId, command, type, title, iconPath, pos, pageId ) VALUES (%s, NULL, NULL, NULL, %d, 0, 0, 7, NULL, NULL, 5, '%q', NULL, %d, %d)",
                                 reserved05, reserved01, b64Title, pos, pageId);
            } else {
                snprintf(sql, sqlSz,
                         "UPDATE tbl_appinfo_icon_n SET pos = %d, pageId = %d WHERE reserved01 = %d",
                         pos, pageId, reserved01);
            }
        } else {
            snprintf(sql, sqlSz,
                     "UPDATE tbl_appinfo_icon_n SET pageId = %d, pos = %d WHERE titleId = '%s'",
                     pageId, pos, id);
        }

        fprintf(logf, "\n\nsql: %s\nid: %s\npageId: %d\npos: %d\ntitle: %s\nr01: %d\nr05: %s\n",
                sql, id, pageId, pos, b64Title, reserved01, reserved05);

        ret = sqlite3_exec(db, sql, NULL, NULL, &error);
        if (ret != SQLITE_OK) {
            snprintf(msgEr, len, "import: icons position sql error\n\n%s", error);
            sqlite3_free(error);
            exitError = 1;
            goto exit;
        }
    }

    curFileLine = 0;

    while (fgets(buff, 250, pageIdBak) != NULL) {
        int scanfC, pageId, pgNo, reserved01;

        memset(sql, 0, sqlSz);

        scanfC = sscanf(buff, "%d %d %d", &pageId, &pgNo, &reserved01);
        if (scanfC != 3) {
            snprintf(msgEr, len, "import pages: failed to parse line: %d", curFileLine);
            goto exit;
        }

        ++curFileLine;

        if (pgNo != -100000000 && pgNo < 0) {
            sqlite3_stmt *stmt = NULL;
            char sqlCountFold[256] = {0};
            int exists = 0;

            snprintf(sqlCountFold, sizeof(sqlCountFold), "SELECT COUNT(*) FROM tbl_appinfo_page_n WHERE pageNo = %d", pgNo);

            ret = sqlite3_prepare_v2(db, sqlCountFold, -1, &stmt, 0);
            if (ret != SQLITE_OK)
                continue;

            if (sqlite3_step(stmt) == SQLITE_ROW)
                exists = sqlite3_column_int(stmt, 0);

            sqlite3_finalize(stmt);

            if (!exists) {
                snprintf(sql, sqlSz,
                         "INSERT INTO tbl_appinfo_page_n (reserved05, reserved04, reserved03, reserved02, reserved01, imageHeight, imageWidth, texHeight, texWidth, bgColor, themeFile, pageNo, pageId ) VALUES (NULL, NULL, NULL, NULL, %d, 0, 0, 0, 0, 0, NULL, %d, %d)",
                         reserved01, pgNo, pageId);
            } else {
                snprintf(sql, sqlSz,
                         "UPDATE tbl_appinfo_page_n SET pageId = %d WHERE pageNo = %d",
                         pageId, pgNo);
            }
        } else {
            snprintf(sql, sqlSz, "UPDATE tbl_appinfo_page_n SET pageId = %d WHERE pageNo = %d", pageId, pgNo);
        }

        ret = sqlite3_exec(db, sql, NULL, NULL, &error);
        if (ret != SQLITE_OK) {
            snprintf(msgEr, len, "import: page ID sql error\n\n%s", error);
            sqlite3_free(error);
            exitError = 1;
            goto exit;
        }
    }

    snprintf(sqlPgNum, sizeof(sqlPgNum), "DELETE FROM tbl_appinfo_page_n WHERE pageNo > %d", (pgNumb - 1));

    ret = sqlite3_exec(db, sqlPgNum, NULL, NULL, &error);
    if (ret != SQLITE_OK) {
        snprintf(msgEr, len, "import: sqlPgNum error\n\n%s", error);
        sqlite3_free(error);
        exitError = 1;
        goto exit;
    }

    // Apply all the changes
    for (int j = 0; j < 13; ++j) {
        ret = sqlite3_exec(db, sqlP1[j], NULL, NULL, &error);
        if (ret != SQLITE_OK) {
            snprintf(msgEr, len, "import: sql error: 1-%d\n\n%s", j, error);
            sqlite3_free(error);
            exitError = 1;
            goto exit;
        }
    }

    ret = copyFile("ux0:iconlayout.ini.bak", "ux0:iconlayout.ini");
    if (!ret) {
        snprintf(msgEr, len, "import: cannot restore iconlayout.ini");
        exitError = 1;
        goto exit;
    }

exit:
    free(buff);
    free(reserved05);
    free(sql);
    fclose(iconsPosBak);
    fclose(pageIdBak);
    fclose(logf);
    sqlite3_close(db);

    if (exitError) {
        copyFile(APPDB, ADB_PATH);
        delete(APPDB);
        copyFile(APPDB_BAK, APPDB);
        delete(APPDB_BAK);

    } else {
        copyFile(APPDB, ADB_PATH);
        delete("ux0:iconlayout.ini.bak");
        delete(APPDB_BAK);
    }

    return !exitError;
}
